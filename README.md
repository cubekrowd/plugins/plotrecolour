# PlotRecolour

Some time ago, PlotSquared switched from the old school formatting codes &a, &l, etc. to a new system called MiniMessage (more info [here](https://docs.adventure.kyori.net/minimessage/format.html)). This broke all the formatting on existing greeting/farewell messages and made them unreadable. This plugin allows you to convert all old school greeting/farewell messages to the new system.

Note that the new format is a lot more verbose, so some converted messages might not fit in PlotSquared's message limit of 255 characters. This plugin logs those converted messages in console and clears the formatting of the flags. This will at least make the greeting/farewell messages readable. By being more efficient with the closing of formats (e.g. remove `</red>` when not necessary, or use `<reset>` for compacter closing), you might be able to manually fix these messages.

This plugin can be used via the command `/plotrecolour` with permission `plotrecolour.use` (though I would recommend running it from the console). The usage is as follows:

1. Run `/plotrecolour` to perform a "dry run", i.e. it will do the conversion except won't actually modify the greeting/farewell messages of all plots. This will print information in console and dump the conversion results in a backup file in the plugin's data folder.
2. Run `/plotrecolour LETSRUMBLE` to do the conversion for realz.
3. Run `/plotrecolour REVERT <world> <backup-file>` in case you need to undo a conversion. Note that this plugin really only supports 1 plot world properly. This isn't safe to use if your server has multiple plot worlds/areas.
