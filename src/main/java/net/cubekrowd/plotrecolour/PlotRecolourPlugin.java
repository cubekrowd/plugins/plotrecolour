package net.cubekrowd.plotrecolour;

import com.plotsquared.core.PlotSquared;
import com.plotsquared.core.plot.Plot;
import com.plotsquared.core.plot.PlotArea;
import com.plotsquared.core.plot.PlotId;
import com.plotsquared.core.plot.flag.implementations.FarewellFlag;
import com.plotsquared.core.plot.flag.implementations.GreetingFlag;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.StringJoiner;
import java.util.logging.Level;
import net.kyori.adventure.text.minimessage.MiniMessage;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

public class PlotRecolourPlugin extends JavaPlugin {
    public boolean needsConversion(String string) {
        var chars = string.toCharArray();
        for (int charIndex = 0; charIndex < chars.length; charIndex++) {
            var ch = chars[charIndex];
            if (ch == '&' && charIndex < chars.length - 1) {
                var code = chars[charIndex + 1];
                if ("0123456789AaBbCcDdEeFfKkLlMmNnOoRrXx".indexOf(code) > -1) {
                    return true;
                }
            }
        }
        return false;
    }

    public String attemptConversion(String string) {
        if (needsConversion(string)) {
            var serializer = LegacyComponentSerializer.builder().character('&').useUnusualXRepeatedCharacterHexFormat().build();
            var component = serializer.deserialize(string).compact();
            var res = MiniMessage.miniMessage().serialize(component);
            return res;
        }
        return null;
    }

    public String stripColourCodes(String string) {
        var res = ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', string));
        return res;
    }

    public void convertPlots(PrintWriter printer, boolean actuallyConvert) {
        var p2 = PlotSquared.get();
        var plotAreas = p2.getPlotAreaManager().getAllPlotAreas();
        for (int areaIndex = 0; areaIndex < plotAreas.length; areaIndex++) {
            var area = plotAreas[areaIndex];
            getLogger().info(String.format("Plot area %s (%s/%s)",
                    area.getWorldName(), areaIndex + 1, plotAreas.length));

            int plotIndex = 0;
            var plots = area.getPlots();
            int plotCount = plots.size();
            int convertIndex = 0;
            int failIndex = 0;

            for (var plot : plots) {
                var flags = plot.getFlagContainer();
                var farewellFlag = flags.getFlag(FarewellFlag.class);
                var greetingFlag = flags.getFlag(GreetingFlag.class);
                var farewell = farewellFlag.getValue();
                var greeting = greetingFlag.getValue();

                if (!farewell.isEmpty() || !greeting.isEmpty()) {
                    var owners = new StringJoiner(", ");
                    for (var owner : plot.getOwners()) {
                        owners.add(Bukkit.getOfflinePlayer(owner).getName());
                    }
                    getLogger().info(String.format("Converting plot %s of %s (%s/%s) (%s)",
                            plot.getId(), owners, plotIndex + 1, plotCount, convertIndex + 1));
                    convertIndex++;

                    var newFarewell = attemptConversion(farewell);
                    var newGreeting = attemptConversion(greeting);

                    boolean failed = false;
                    if (newFarewell != null && newFarewell.length() > 250) {
                        failed = true;
                        getLogger().warning(String.format("New farewell too long: %s. Removing colours (%s)\n%s", newFarewell.length(), failIndex + 1, newFarewell));
                        newFarewell = stripColourCodes(farewell);
                    }
                    if (newGreeting != null && newGreeting.length() > 250) {
                        failed = true;
                        getLogger().warning(String.format("New greeting too long: %s. Removing colours (%s)\n%s", newGreeting.length(), failIndex + 1, newGreeting));
                        newGreeting = stripColourCodes(greeting);
                    }

                    if (failed) {
                        failIndex++;
                    }

                    printer.println(String.format(Locale.ENGLISH, "plot %s", plot.getId()));

                    if (newFarewell != null) {
                        printer.println(String.format(Locale.ENGLISH, "- farewell:\n%s", farewell));
                        printer.println(String.format(Locale.ENGLISH, "+ farewell:\n%s", newFarewell));
                    }
                    if (newGreeting != null) {
                        printer.println(String.format(Locale.ENGLISH, "- greeting:\n%s", greeting));
                        printer.println(String.format(Locale.ENGLISH, "+ greeting:\n%s", newGreeting));
                    }
                    printer.println();

                    if (printer.checkError()) {
                        getLogger().warning("Something went wrong while writing to the backup file. Stopping.");
                        return;
                    }

                    if (newFarewell != null && actuallyConvert) {
                        var newFlag = farewellFlag.createFlagInstance(newFarewell);
                        plot.setFlag(newFlag);
                    }
                    if (newGreeting != null && actuallyConvert) {
                        var newFlag = greetingFlag.createFlagInstance(newGreeting);
                        plot.setFlag(newFlag);
                    }
                }

                plotIndex++;
            }
        }
    }

    public String readMessageLines(ListIterator<String> iter) {
        var res = new StringJoiner("\n");
        while (iter.hasNext()) {
            var line = iter.next();
            if (line.startsWith("- farewell:") || line.startsWith("+ farewell:") || line.startsWith("- greeting:") || line.startsWith("+ greeting:") || line.isBlank()) {
                iter.previous();
                break;
            }
            res.add(line);
        }
        return res.toString();
    }

    public boolean readGreetingOrFarewell(Plot plot, ListIterator<String> iter) {
        if (!iter.hasNext()) {
            return false;
        }
        var next = iter.next();
        if (next.isBlank()) {
            return false;
        }
        if (next.startsWith("- farewell:")) {
            getLogger().info("Reverting farewell message");
            var farewell = readMessageLines(iter);
            var newFlag = plot.getFlagContainer().getFlag(FarewellFlag.class).createFlagInstance(farewell);
            plot.setFlag(newFlag);
            return true;
        } else if (next.startsWith("+ farewell:")) {
            readMessageLines(iter);
            return true;
        } else if (next.startsWith("- greeting:")) {
            getLogger().info("Reverting greeting message");
            var greeting = readMessageLines(iter);
            var newFlag = plot.getFlagContainer().getFlag(GreetingFlag.class).createFlagInstance(greeting);
            plot.setFlag(newFlag);
            return true;
        } else if (next.startsWith("+ greeting:")) {
            readMessageLines(iter);
            return true;
        }
        return false;
    }

    public void revertFromFile(PlotArea area, List<String> lines) {
        var iter = lines.listIterator();
        while (iter.hasNext()) {
            var plotLine = iter.next();
            if (!plotLine.startsWith("plot ")) {
                getLogger().warning("Unexpected line: " + plotLine);
                return;
            }
            var plotId = plotLine.replace("plot ", "");
            var plot = area.getPlot(PlotId.fromString(plotId));
            getLogger().info(String.format("Reverting plot %s", plot.getId()));
            for (;;) {
                if (!readGreetingOrFarewell(plot, iter)) {
                    break;
                }
            }
        }
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        var now = OffsetDateTime.now(ZoneOffset.UTC);
        var backupFile = new File(getDataFolder(), "backup-" + now.format(DateTimeFormatter.ofPattern("yyyy-MM-dd_HH.mm.ss", Locale.ENGLISH)) + ".txt");

        boolean actuallyConvert = false;
        if (args.length > 0 && args[0].equals("LETSRUMBLE")) {
            actuallyConvert = true;
        }

        if (args.length > 2 && args[0].equals("REVERT")) {
            var worldName = args[1];
            var foundAreas = PlotSquared.get().getPlotAreaManager().getPlotAreasSet(worldName);
            if (foundAreas.size() != 1) {
                sender.sendMessage("Couldn't find an accurate plot area match for that world.");
                return true;
            }
            var area = foundAreas.iterator().next();

            var revertFile = new File(getDataFolder(), args[2]);
            try {
                var lines = Files.readAllLines(revertFile.toPath());
                revertFromFile(area, lines);
            } catch (IOException e) {
                getLogger().log(Level.WARNING, "Failed to read file", e);
            }
            return true;
        }

        try {
            backupFile.getParentFile().mkdirs();
            if (!backupFile.createNewFile()) {
                sender.sendMessage("Backup file already exists for the current time. Try again in a second.");
                return true;
            }
        } catch (IOException e) {
            getLogger().log(Level.WARNING, "Failed to create backup file", e);
            sender.sendMessage("Failed to create backup file. Stopping.");
            return true;
        }

        try (var printer = new PrintWriter(new BufferedWriter(new FileWriter(backupFile, StandardCharsets.UTF_8)))) {
            convertPlots(printer, actuallyConvert);
        } catch (IOException e) {
            getLogger().log(Level.WARNING, "Failed to write to backup file", e);
        }
        return true;
    }
}
